<?php

namespace Harmony\Composer;

use Composer\Installer\LibraryInstaller;
use Composer\Package\PackageInterface;
use Composer\Util\Filesystem;

class HarmonyInstaller extends LibraryInstaller
{
    /**
     * {@inheritDoc}
     */
    public function getInstallPath(PackageInterface $package)
    {
        if ('harmony-module' == $package->getType()) {
            list($vendor, $package) = explode('/', $package->getName());

            return 'modules/'.$vendor.'/'.$package;
        }
    }

    /**
     * {@inheritDoc}
     */
    public function supports($packageType)
    {
        return 'harmony-module' === $packageType;
    }
}
