# Harmony Module Installer

Composer plugin for automatic installation of Harmony Platform modules.

## Usage

```bash
composer require --dev harmony/module-installer
```
